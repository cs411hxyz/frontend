// requires 'js/sha1.js' jQuery

// Upon successful login or sign-up, if body's class is NOT ".THIS-IS-ERROR-PAGE", then refresh current page.
//      OTHERWISE: go to homepage.

$("#signupModal .form-signin").submit(function () {
  $("#signupModal .authFailure").hide();

  var username = $("#signupModal input[type='username']").val();
  var password =
    CryptoJS.SHA1($("#signupModal input[type='password']").val())
    .toString(CryptoJS.enc.Base64);

  $.ajax({
    method: 'POST',
    url: restServerUrl + "/auth/signup",
    data: {
      'username': username,
      'password': password,
      'dryrun': false
    },
    success: function (data) {
      Cookies.set('auth-token', data['auth-token']);
      if($("body").hasClass("THIS-IS-ERROR-PAGE"))
        window.location.href = homepage;
      else location.reload();
    },
    error: function (err) {
      $("#signupModal .authFailure").text(err.responseJSON.error);
      $("#signupModal .authFailure").show();
    }
  });
  return false;
});

$("#loginModal .form-signin").submit(function () {
  $("#loginModal .authFailure").hide();

  var username = $("#loginModal input[type='username']").val();
  var password =
    CryptoJS.SHA1($("#loginModal input[type='password']").val())
    .toString(CryptoJS.enc.Base64);

  $.ajax({
    method: 'POST',
    url: restServerUrl + "/auth/login",
    data: {
      'username': username,
      'password': password
    },
    success: function (data) {
      Cookies.set('auth-token', data['auth-token']);
      if($("body").hasClass("THIS-IS-ERROR-PAGE"))
        window.location.href = homepage;
      else location.reload();
    },
    error: function (err) {
      $("#loginModal .authFailure").text(err.responseJSON.error);
      $("#loginModal .authFailure").show();
    }
  });
  return false;
});

$("#logoutBtn").click(function() {
  Cookies.remove('auth-token');
  window.location.href = homepage;
});
