// Basically this scripts gets info from the panels $(".add-to-sidebar") and adds title and link to $("ul.sidebar")

$("#navbarSearchForm").show();

var gparent  = $("#generate-container");
var gtemp    = $("#generate-template");
var gcount   = $("#generate-count");
var gkeyword = $("#generate-keyword");    // remember that 'g' represents generated. Thus this is not the source of keyword

var numCheckedTweets = 0;

function preSearch()
{
  gparent.hide();
  $('.waitMessage').show();
  $('.showAfterSearch').hide();
  $('.errMessage').hide();

  $("#navbarRelevSearchBtn").prop('disabled', 'true');
  $("#navbarRelevSearchBtn").attr('disabled', 'true');
  $("#navbarRelevSearchBtn").addClass('btn-default');
  $("#navbarRelevSearchBtn").removeClass('btn-success');
}

function searchSuccess(key, data)
{
  gparent.empty();
  gcount.text(data.length);
  gkeyword.text(key);
  for(var i=0; i<data.length; i++) {
    var item = data[i];

    var view = gtemp.clone();
    view.removeAttr('id');
    view.find(".category").text(item.category);
    view.find(".time").text(item.timestamp);
    view.find(".content").text(item.content);
    view.find(".likes").text(item.numfollowers);
    if(item.url) {
      view.find(".urllink").attr('href', item.url);
      view.find(".urllink-container").show();
    }
    view.show();
    gparent.append(view);
  }
  $('.waitMessage').hide();

  numCheckedTweets = 0;
  $(".search-result .likesBtn").each(function() {
    var btn = this;
    $(btn).on("click", function () {
      var chkbx = $($(btn).find("input[type='checkbox']"));
      chkbx.prop('checked', !chkbx.prop('checked'));
      if(chkbx.prop('checked'))
        numCheckedTweets++;
      else numCheckedTweets--;
      if(numCheckedTweets > 0)
      {
        $("#navbarRelevSearchBtn").removeProp('disabled');
        $("#navbarRelevSearchBtn").removeAttr('disabled');
        $("#navbarRelevSearchBtn").removeClass('btn-default');
        $("#navbarRelevSearchBtn").addClass('btn-success');
      }
      else
      {
        $("#navbarRelevSearchBtn").prop('disabled', 'true');
        $("#navbarRelevSearchBtn").attr('disabled', 'true');
        $("#navbarRelevSearchBtn").addClass('btn-default');
        $("#navbarRelevSearchBtn").removeClass('btn-success');
      }
    });
  });

  gparent.show();
  $('.showAfterSearch').show();
}

function searchFail(err) {
  if(err.responseJSON)
    $('.errMessage .lead').text(err.responseJSON.error);
  else $('.errMessage .lead').text("Internet Problem.");
  $('.waitMessage').hide();
  $('.errMessage').show();
}

$("#navbarSearchBtn").click(function() {
  if($(this).prop('disabled') || $(this).attr('disabled'))
      return;
  preSearch();
  var key = $("#navbarSearchText").val();
  var category = $("#navbarCategoryText").val();
  var minfollow = $("#navbarFollowFilter").val();

  $.ajax({
    method: 'POST',
    url: restServerUrl + "/search",
    data: {
      'auth-token': Cookies.get('auth-token'),
      'query': key,
      'category': category,
      'min_num_followers': minfollow
    },
    success: searchSuccess.bind(null, key),
    error: searchFail
  });
  return false;
});

$("#navbarRelevSearchBtn").click(function() {
  if($(this).prop('disabled') || $(this).attr('disabled'))
      return;
  preSearch();
  var key = $("#navbarSearchText").val();
  var category = $("#navbarCategoryText").val();
  var minfollow = $("#navbarFollowFilter").val();
  var fb = [];

  $("article.search-result").each(function() {
    var row = this;
    var chkbx = $($(row).find(".likesBtn input[type='checkbox']"));
    if(chkbx.prop("checked"))
      fb.push($(row).find("p.content").text());
  });

  $.ajax({
    method: 'POST',
    url: restServerUrl + "/search",
    data: {
      'auth-token': Cookies.get('auth-token'),
      'query': key,
      'category': category,
      'min_num_followers': minfollow,
      'feedback': "True",
      'feedback_tweets': fb
    },
    success: searchSuccess.bind(null, key),
    error: searchFail
  });
  return false;
});
