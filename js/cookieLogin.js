// this library should be placed at the end of html body
// It detects from cookies whether the browser has stored a valid token
// If so, it will login.

// jQuery and js-cookie is needed.

// If the library can login, it will set all tags with class 'nologin' to hide.
// Otherwise it will set those with class 'havelogin' to hide.

// Calling convention:
//      .havelogin <=> fields that are shown only when logged in
//      .nologin <=> fields that are shown only when NOT logged in
//      $(document).on('afterCookieLogin', (name, uid)) <=> SUCCESSFUL login.


var username;
var userid;

function loginByCookieFailed() {
    userid = null;
    username = null;
    $(".havelogin").hide();
    $(".nologin").show();
    $(document).trigger('cookieLoginFailure', []);
}

function loginByCookieSuccess() {
    $(".havelogin").show();
    $(".nologin").hide();
    $(document).trigger('afterCookieLogin', [username, userid]);
}

if (!Cookies.get('auth-token')) {
    userid = null;
    username = null;
    loginByCookieFailed();
} else {
    $.ajax({
        method: 'GET',
        data: {
            'auth-token': Cookies.get('auth-token')
        },
        url: restServerUrl + "/me",
        success: function (data) {
            userid = data.userid;
            username = data.username;
            loginByCookieSuccess();
        },
        error: function (err) {
            // This will be a network or server error.
            loginByCookieFailed();
        }
    });
}
