// This file, when added, checks whether the user has logged in, if not, it will redirect to /needLogin.shtml page -->

/*
    This file utilizes the triggers defined in cookieLogin.js, which is already loaded IN navbar.shtml Therefore:
        1: This file does not load cookieLogin.js
        2: This file must be included IN HEADER! (Thus before including navbar.shtml)
*/

$(document).on("cookieLoginFailure", function() {
    window.location.href = "/needLogin.shtml";
});
