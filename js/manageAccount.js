// Basically this scripts gets info from the panels $(".add-to-sidebar") and adds title and link to $("ul.sidebar")

var sidebar = $("ul.sidebar");

$(".add-to-sidebar").each(function(id, panel) {
  panel.id = "sidebar-nav" + id;

  var newItem = $("<li>");
  var itemLink = $("<a>");
  itemLink.text($(panel).find(".panel-title").text());
  itemLink.attr("href", "#" + panel.id);
  newItem.append(itemLink);
  newItem.addClass("list-group-item");
  sidebar.append(newItem);
});

$("#modPassword .form-signin").submit(function () {
  $("#modPassword .authFailure").hide();
  $("#modPassword .authSuccess").hide();

  var oldpass = $("#modPassword input.oldpass").val();
  var newpass = $("#modPassword input.newpass").val();
  var confirmpass = $("#modPassword input.confirmpass").val();
  if (newpass != confirmpass) {
    $("#modPassword .authFailure").text("Your password and confirmation password do not match.");
    $("#modPassword .authFailure").show();
    return false;
  }

  oldpass = CryptoJS.SHA1(oldpass).toString(CryptoJS.enc.Base64);
  newpass = CryptoJS.SHA1(newpass).toString(CryptoJS.enc.Base64);

  $.ajax({
    method: 'POST',
    url: restServerUrl + "/me/modify",
    data: {
      'auth-token': Cookies.get('auth-token'),
      'old-pass': oldpass,
      'new-pass': newpass
    },
    success: function (data) {
      $("#modPassword .authSuccess").show();
    },
    error: function (err) {
      $("#modPassword .authFailure").text(err.responseJSON.error);
      $("#modPassword .authFailure").show();
    }
  });
  return false;
});


$("#delAccount .form-signin").submit(function () {
  $("#delAccount .authFailure").hide();
  $("#delAccount .authSuccess").hide();

  var username = $("#delAccount input.username").val();
  var password = $("#delAccount input.password").val();

  password = CryptoJS.SHA1(password).toString(CryptoJS.enc.Base64);

  $.ajax({
    method: 'POST',
    url: restServerUrl + "/me/delete",
    data: {
      'auth-token': Cookies.get('auth-token'),
      'username': username,
      'password': password
    },
    success: function (data) {
      $("#delAccount .authSuccess").show();
      window.setTimeout(function () {
        window.location.href = homepage;
      }, 2000);
    },
    error: function (err) {
      $("#delAccount .authFailure").text(err.responseJSON.error);
      $("#delAccount .authFailure").show();
    }
  });
  return false;
});

// Fetch username
$.ajax({
  method: 'GET',
  url: restServerUrl + "/me",
  data: {
    'auth-token': Cookies.get('auth-token'),
  },
  success: function (data) {
    if(data.username)
      $("#first-name").val(data.username);
    else $("#first-name").val("404 Not found.");
  },
  error: function (err) {
    $("#first-name").val("404 Not found.");
  }
});
