#### This site is almost fully fronted

The only dynamic backend language is SHTML, because that way we can write webpages into modules and compose website by including modules.

It does not directly operate SQL and uses RESTful API defined on another backend to communicate with TextMining results and Databases.